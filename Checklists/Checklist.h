//
//  Checklist.h
//  Checklists
//
//  Created by demo on 7/6/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Checklist : NSObject <NSCoding>

@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, copy) NSString *iconName;

- (int)countUncheckedItems;

@end
