//
//  ChecklistsAppDelegate.h
//  Checklists
//
//  Created by demo on 6/27/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChecklistsAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
