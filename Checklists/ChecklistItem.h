//
//  ChecklistItem.h
//  Checklists
//
//  Created by demo on 7/3/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChecklistItem : NSObject <NSCoding>

@property (nonatomic, copy) NSString *text;
@property (nonatomic, assign) BOOL checked;

- (void)toggleChecked;

@end
