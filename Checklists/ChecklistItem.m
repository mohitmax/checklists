//
//  ChecklistItem.m
//  Checklists
//
//  Created by demo on 7/3/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import "ChecklistItem.h"

@implementation ChecklistItem

@synthesize text;
@synthesize checked;


//Method from the protocol NSCoder to READ from the saved plist and load it into the app.
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        self.text = [aDecoder decodeObjectForKey:@"Text"];
        self.checked = [aDecoder decodeBoolForKey:@"Checked"];
    }
    return self;
}

//Method from the protocol NSCoder to SAVE new/edited data into the plist file.
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.text forKey:@"Text"];
    [aCoder encodeBool:self.checked forKey:@"Checked"];
}

- (void)toggleChecked
{
    self.checked = !self.checked;
}

@end
