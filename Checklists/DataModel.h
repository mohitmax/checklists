//
//  DataModel.h
//  Checklists
//
//  Created by demo on 7/8/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataModel : NSObject

@property (nonatomic, strong) NSMutableArray *lists;

- (void)saveChecklists;
- (int)indexOfSelectedChecklist;
- (void)setIndexOfSelectedChecklist:(int)index;
- (void)sortChecklists;

@end
