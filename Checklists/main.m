//
//  main.m
//  Checklists
//
//  Created by demo on 6/27/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ChecklistsAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ChecklistsAppDelegate class]));
    }
}
