//
//  ChecklistsViewController.h
//  Checklists
//
//  Created by demo on 6/27/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemDetailViewController.h"

@class Checklist;

@interface ChecklistViewController : UITableViewController <ItemDetailViewControllerDelegate>

@property (nonatomic, strong) Checklist *checklist;

@end
